@echo off


setlocal enabledelayedexpansion
pushd %~dp0
echo EXECUTING TRANSFORMATION ["RN_ALL_TICKETS"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_RN\ALL_TICKETS\RN_ALL_TICKETS.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished executing transformation - !time! *
ECHO *************************************************
popd
