@echo off


setlocal enabledelayedexpansion
pushd %~dp0
echo EXECUTING TRANSFORMATION ["ETL_RN_ENCUESTAS"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_RN\ETL_RN_ENCUESTAS.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished executing transformation - !time! *
ECHO *************************************************
popd
