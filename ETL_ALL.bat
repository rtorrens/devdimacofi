@echo off

echo EXECUTING ALL TRANSFORMATIONS 
setlocal enabledelayedexpansion


ECHO *****************START DATA DRIVEN ********************************

pushd %~dp0
ECHO *************************************************
ECHO START DATADRIVEN_ETL executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["ETL_DATADIVEN.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_SAI\DATADRIVEN_ETL.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished DATADRIVEN_ETL executing transformation - !time! *
ECHO *************************************************
popd


pushd %~dp0
ECHO *************************************************
ECHO START DATADRIVEN_ETL_12 executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["DATADRIVEN_ETL_12.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_SAI\DATADRIVEN_ETL_12.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished DATADRIVEN_ETL_12 executing transformation - !time! *
ECHO *************************************************
popd

pushd %~dp0
ECHO *************************************************
ECHO START DATADRIVEN_ETL_MES executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["DATADRIVEN_ETL_MES.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_SAI\DATADRIVEN_ETL_MES.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished executing transformation - !time! *
ECHO *************************************************
popd

pushd %~dp0
ECHO *************************************************
ECHO START ETL_DATADRIVEN_OPORTUNIDADES_ETL executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["ETL_DATADRIVEN_OPORTUNIDADES_ETL.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_SAI\ETL_DATADRIVEN_OPORTUNIDADES_ETL.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished ETL_DATADRIVEN_OPORTUNIDADES_ETL executing transformation - !time! *
ECHO *************************************************
popd

ECHO *************************************************
ECHO PARTE TRANSFORMACIOENS DE ETL TICKETS
ECHO *************************************************

pushd %~dp0
ECHO *************************************************
ECHO START RN_ALL_TICKETS executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["RN_ALL_TICKETS"]  TODOS LOS TICKETS
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_RN\ALL_TICKETS\RN_ALL_TICKETS.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished RN_ALL_TICKETS executing transformation - !time! *
ECHO *************************************************
popd

pushd %~dp0
ECHO *************************************************
ECHO START ALL_HIS_TICKETS executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["ALL_HIS_TICKETS"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_RN\ALL_HIS_TICKETS\ALL_HIS_TICKETS.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished ALL_HIS_TICKETS executing transformation - !time! *
ECHO *************************************************
popd


pushd %~dp0
ECHO *************************************************
ECHO START RN_TICKETS_ALL_LOOP executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["RN_TICKETS_ALL_LOOP.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_RN\DATADRIVEN\RN_TICKETS_ALL_LOOP.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished RN_TICKETS_ALL_LOOP executing transformation - !time! *
ECHO *************************************************
popd

ECHO ******************END TICKETS *******************************


ECHO ******************STAT EBS *******************************
pushd %~dp0
ECHO *************************************************
ECHO START ETL_EBS_TOTAL executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["ETL_EBS_TOTAL.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_EBS\ETL_EBS_TOTAL.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished ETL_EBS_TOTAL executing transformation - !time! *
ECHO *************************************************
popd
ECHO ******************END  EBS *******************************


ECHO ******************START DDDM*******************************

pushd %~dp0
ECHO *************************************************
ECHO START DDDM executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["D:\pentaho\pdi-ce-8.1.0.0-365\DDDM"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\pdi-ce-8.1.0.0-365\DDDM\DDDM.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished DDDM executing transformation - !time! *
ECHO *************************************************
popd



