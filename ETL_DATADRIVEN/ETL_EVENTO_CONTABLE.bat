@echo off

echo EXECUTING ALL TRANSFORMATIONS 
setlocal enabledelayedexpansion


ECHO *****************START DATA DRIVEN ********************************

pushd %~dp0
ECHO *************************************************
ECHO START ETL_EVENTOS_CONTABLES executing transformation - !time! * 
ECHO *************************************************
echo EXECUTING TRANSFORMATION ["ETL_EVENTOS_CONTABLES.kjb"]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_DATADRIVEN\ETL_EVENTOS_CONTABLES.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished ETL_EVENTOS_CONTABLES executing transformation - !time! *
ECHO *************************************************
popd

