@echo off


setlocal enabledelayedexpansion
pushd %~dp0
echo EXECUTING TRANSFORMATION ["ETL_SAI_GESTION_NC]
call D:\pentaho\pdi-ce-8.1.0.0-365\data-integration\Kitchen.bat -file:"D:\pentaho\devDimacofi\ETL_DATADRIVEN\JOBS\ETL_SAI_GESTION_NC.kjb" -level:Minimal
ECHO *************************************************
ECHO Finished executing transformation - !time! *
ECHO *************************************************
popd
